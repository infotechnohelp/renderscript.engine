<?php declare(strict_types=1);

namespace RenderScript\Lib\Recipe;

use RenderScript\Lib\Component;
use RenderScript\Lib\Recipe\ComponentStructure\Base as ComponentStructureBase;

// @todo ??? extends DataContainer
class ComponentStructure extends ComponentStructureBase
{
    private function collectAllRegisteredComponentTitles(): array
    {
        $componentTitles = [];

        $this->getChildrenComponentTitles($this->getData()['children'], $componentTitles);

        return $componentTitles;
    }

    private function getChildrenComponentTitles(array $data, array &$componentTitles)
    {
        foreach ($data as $componentData) {

            /** @var Component $Component */
            $Component = $componentData['component'];

            $componentTitles[] = $Component->getComponentTitle();

            if (isset($componentData['children']) && !empty($componentData['children'])) {
                $this->getChildrenComponentTitles($componentData['children'], $componentTitles);
            }
        }
    }

    public function validate()
    {
        $allComponentTitles = $this->collectAllRegisteredComponentTitles();

        $counts = array_count_values($allComponentTitles);
        foreach ($counts as $name => $count) {
           if ($count > 1) {
               throw new \Exception("Duplicated component structure item '$name'");
           }
        }
    }
}