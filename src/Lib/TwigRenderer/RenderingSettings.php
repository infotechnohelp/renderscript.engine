<?php declare(strict_types=1);

namespace RenderScript\Lib\TwigRenderer;

use RenderScript\Lib\Component\Data as ComponentData;

class RenderingSettings extends ComponentData
{
    public function __construct()
    {
        parent::__construct(null);

        $this
            ->escapePhpTag(false);
    }

    public function escapePhpTag(bool $value = true): self
    {
        $this->set('escapePhpTag', $value);

        return $this;
    }
}