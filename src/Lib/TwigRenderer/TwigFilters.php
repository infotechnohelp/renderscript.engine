<?php declare(strict_types=1);

namespace RenderScript\Lib\TwigRenderer;

use Cake\Utility\Inflector;
use Twig\Environment;
use Twig\TwigFilter;
use VendorTmp\TextModifier\StringModifier;

class TwigFilters
{
    private Inflector $Inflector;

    public function __construct()
    {
        $this->Inflector = new Inflector();
    }

    public function addFiltersToTwig(Environment $twig)
    {
        $twig->addFilter(new TwigFilter('ltrim', function ($value, $characters) {
            return ltrim($value, $characters);
        }));

        $twig->addFilter(new TwigFilter('ltrimDuplicates', function ($value, $characters) {
            return StringModifier::ltrimDuplicates($value, $characters);
        }));

        $twig->addFilter(new TwigFilter('underscore', function ($string) {
            return $this->Inflector::underscore($string);
        }));

        $twig->addFilter(new TwigFilter('camelize', function ($string) {
            return $this->Inflector::camelize($string);
        }));

        $twig->addFilter(new TwigFilter('dasherize', function ($string) {
            return $this->Inflector::dasherize($string);
        }));

        $twig->addFilter(new TwigFilter('humanize', function ($string) {
            return $this->Inflector::humanize($string);
        }));

        $twig->addFilter(new TwigFilter('pluralize', function ($string) {
            return $this->Inflector::pluralize($string);
        }));

        $twig->addFilter(new TwigFilter('singularize', function ($string) {
            return $this->Inflector::singularize($string);
        }));

        $twig->addFilter(new TwigFilter('print_r', function ($value) {
            return print_r($value, true);
        }));

        $twig->addFilter(new TwigFilter('lcfirst', function ($value) {
            return lcfirst($value);
        }));

        $twig->addFilter(new TwigFilter('ucfirst', function ($value) {
            return ucfirst($value);
        }));

        $twig->addFilter(new TwigFilter('boolean', function ($value) {
            return filter_var($value, FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false';
        }));

        $twig->addFilter(new TwigFilter('if_boolean', function ($value) {
            if (!is_bool($value) && !in_array($value, [0, 1, '0', '1'], true)) {
                return $value;
            }

            return filter_var($value, FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false';
        }));
    }
}