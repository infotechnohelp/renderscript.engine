<?php declare(strict_types=1);

namespace RenderScript\Lib\Component;

// @todo ??? Extends DataContainer
class Data implements \JsonSerializable
{
    private array $data = [];

    private ?string $componentTitle = null;

    public function __construct(string $componentTitle = null)
    {
        $this->componentTitle = $componentTitle;
    }

    public function getComponentTitle(): ?string
    {
        return $this->componentTitle;
    }

    protected function set(string $key, $value)
    {
        $this->data[$key] = $value;
    }

    protected function get(string $key){
        return $this->data[$key] ?? null;
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}