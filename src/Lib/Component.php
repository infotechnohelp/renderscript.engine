<?php declare(strict_types=1);

namespace RenderScript\Lib;

use Cake\Utility\Inflector;
use Infotechnohelp\TextModifier\StringModifier;
use RenderScript\Lib\Component\Data as ComponentData;

class Component implements \JsonSerializable
{
    protected ?string $enginePackageTitle = null;

    private string $template;

    private string $componentTitle;

    private string $defaultComponentTitle;

    private array $reservedIncludes = [];

    private ?Component $parent = null;

    private ?ComponentData $data = null;

    private array $components = [];

    private bool $isApi = false; // No need ???


    // @todo Think carefully about parameters
    public function __construct(string $__CLASS__, string $customTitle = null, bool $isApi = false, bool $isBase = false)
    {
        $this->isApi = $isApi;

        $prefix = $this->prepareComponentTitlePrefix($isApi, $isBase);

        $this->validateComponentTitle($__CLASS__, $prefix);

        $componentTitle = $this->prepareComponentTitle($__CLASS__, $prefix);

        $this->defaultComponentTitle = $componentTitle;
        $this->componentTitle = $customTitle ?? $componentTitle;

        $this->template = $this->prepareTemplatePath($__CLASS__, $prefix, $isBase);
    }

    private function prepareComponentTitle(string $__CLASS__, string $prefix)
    {
        $class = substr($__CLASS__, strlen($prefix));

        $exploded = explode('\\', $class);

        return implode('.', $exploded);
    }

    protected function parsePackageTitle()
    {
        $composerData = json_decode(file_get_contents(dirname(__DIR__, 2) . '/composer.json'));

        return $composerData->name;
    }

    private function prepareTemplatePath(string $__CLASS__, string $prefix, bool $isBase = false)
    {
        $class = substr($__CLASS__, strlen($prefix));

        $exploded = explode('\\', $class);

        $templateTitleData = $exploded;

        array_walk($templateTitleData, function (&$value) {
            $value = Inflector::underscore(StringModifier::lcfirst($value, '_'));
        });


        $pathPrefix = "vendor/{$this->parsePackageTitle()}/";
        
        $templatePathPrefix = ($isBase) ?
            $pathPrefix . 'src/Lib/templates/' :
            $pathPrefix . 'src/Extension/templates/';


        if ($this->isApi) {
            $templatePathPrefix = 'src/Extension/templates/';
        }

        return $templatePathPrefix . implode("/", $templateTitleData) . ".twig";
    }

    private function prepareComponentTitlePrefix(bool $isApi = false, bool $isBase = false)
    {
        $prefix = ($isBase) ? "RenderScript\Lib\Component\\" : "RenderScript\Extension\Component\\";

        return ($isApi) ? "RenderScript\Api\Extension\Component\\" : $prefix;
    }

    private function validateComponentTitle(string $__CLASS__, string $prefix)
    {
        if (strpos($__CLASS__, $prefix) !== 0) {
            throw new \Exception("$__CLASS__ is not a correct component class");
        }
    }

    public function isBase()
    {
        return $this->parent === null;
    }

    public function getComponentTitle(): string
    {
        return $this->componentTitle;
    }

    public function getDefaultComponentTitle(): string
    {
        return $this->defaultComponentTitle;
    }


    public function reserveIncluded(Component $component): self
    {
        $this->reservedIncludes[] = $component->getComponentTitle();

        return $this;
    }

    public function setParent(Component $component): self
    {
        $this->parent = $component;

        return $this;
    }

    public function addComponent(Component $component): self
    {
        $this->components[$component->getComponentTitle()] =
            $component
                ->setParent($this);

        return $this;
    }

    private function throwException(string $msg)
    {
        throw new \Exception($msg . ': ' . json_encode($this->jsonSerialize(false)));
    }

    private function componentIncluded(string $title): bool
    {
        $components = ($this->parent === null) ? $this->components : $this->parent->components;

        return array_key_exists($title, $components);
    }

    private function validateIncluded(): void
    {
        foreach ($this->reservedIncludes as $reservedInclude) {
            if (!$this->componentIncluded($reservedInclude)) {
                $this->throwException("Component '$reservedInclude' missing");
            }
        }
    }

    private function validate(): void
    {
        if ($this->parent !== null && $this->template === 'src/Lib/templates/base.twig') {
            $this->throwException('Template is not set');
        }

        $this->validateIncluded();
    }

    public function setData(ComponentData $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function initDefaultData()
    {
        $dataClassName =
            str_replace('RenderScript\\Extension\\Component', 'RenderScript\\Extension\\ComponentData', get_class($this));

        // @todo What about API?

        return new $dataClassName();
    }

    public function getTemplatePath()
    {
        return $this->template;
    }

    public function jsonSerialize(bool $validate = true)
    {
        if ($validate) {
            $this->validate();
        }

        return [
            'config' => [
                'renderscript' => [
                    'enginePackageTitle' => $this->enginePackageTitle,
                ],
                'template' => $this->template,
                'include' => $this->reservedIncludes,
            ],
            'data' => $this->data,
            'components' => $this->components,
        ];
    }

    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }

}