<?php declare(strict_types=1);

namespace RenderScript\Lib;

use RenderScript\Lib\TwigRenderer\RenderingSettings;
use RenderScript\Lib\TwigRenderer\TwigFilters;
use Twig\Environment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;

class TwigRenderer
{
    private Environment $twig;

    protected RenderingSettings $settings;

    public function __construct(RenderingSettings $settings = null)
    {
        $rootDir = dirname(__DIR__, 5) . '/';
        
        $loader = new TwigFilesystemLoader($rootDir);
        $this->twig = new Environment($loader, [
            'cache' => $rootDir . 'tmp/compilation_cache',
            'debug' => true,
        ]);

        (new TwigFilters())->addFiltersToTwig($this->twig);

        $this->settings = ($settings === null) ? new RenderingSettings() : $settings;
    }

    public function renderBaseComponent(Component $component, RenderingSettings $settings = null): ?string
    {
        $settings = ($settings === null) ? $this->settings : $settings;

        if (!$component->isBase()) {
            throw new \Exception('Component has parents'); // Create ComponentException later (separate file)
        }

        $data = array_merge($component->toArray(), [
            'settings' => $settings->jsonSerialize()
        ]);

        return $this->twig->render($component->getTemplatePath(), $data);
    }
}