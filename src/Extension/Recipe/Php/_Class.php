<?php declare(strict_types=1);

namespace RenderScript\Extension\Recipe\Php;

use RenderScript\Extension\Component\Php\_Class\Getters;
use RenderScript\Extension\Component\Php\_Class\Properties;
use RenderScript\Extension\Component\Php\_Class\Setters;
use RenderScript\Lib\Recipe\ComponentStructure;
use RenderScript\Lib\Recipe\ComponentStructure\Item as Item;
use RenderScript\Lib\Recipe as BaseRecipe;
use RenderScript\Lib\Recipe\RecipeInterface;

use RenderScript\Extension\Component\Php\File\Base as Base;
use RenderScript\Extension\Component\Php\File\Watermark as Watermark;
use RenderScript\Extension\Component\Php\File\_Namespace as _Namespace;
use RenderScript\Extension\Component\Php\File\_Use as _Use;

use RenderScript\Extension\Component\Php\_Class\Base as PhpClassBase;

class _Class extends BaseRecipe implements RecipeInterface
{
    public function getRecipeComponentStructure(): ?ComponentStructure
    {
        return (new ComponentStructure())
            ->addChild(
                (new Item(new Base()))
                    ->addChild(new Item(new Watermark()))
                    ->addChild(new Item(new _Namespace()))
                    ->addChild(new Item(new _Use()))
            )
            ->addChild(
                (new Item((new PhpClassBase())))
                ->addChild((new Item(new Properties())))
                ->addChild((new Item(new Getters())))
                ->addChild((new Item(new Setters())))
            );
    }
}