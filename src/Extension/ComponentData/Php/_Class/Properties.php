<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Properties extends ComponentData implements \JsonSerializable
{
    private PropertiesDefault $default;

    private array $properties = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new PropertiesDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(PropertiesDefault $Default): self
    {
        $this->default = $Default;

        return $this;
    }

    public function getDefault(): PropertiesDefault
    {
        return $this->default;
    }

    public function Property()
    {
        return new Property();
    }

    public function addProperty(string $title, Property $Property = null): self
    {
        $Property = ($Property === null) ? new Property() : $Property;

        $this->properties[$title] = $Property;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'properties' => $this->properties,
        ];
    }

}