<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Setters extends ComponentData
{
    private SettersDefault $default;

    private array $setters = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new SettersDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(SettersDefault $Default): self
    {
        $this->default = $Default;

        return $this;
    }

    public function getDefault(): SettersDefault
    {
        return $this->default;
    }

    public function Setter()
    {
        return new Setter();
    }

    public function addSetter(Setter $Setter = null): self
    {
        $Setter = ($Setter === null) ? new Setter() : $Setter;

        $this->setters[] = $Setter;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'setters' => $this->setters,
        ];
    }

}