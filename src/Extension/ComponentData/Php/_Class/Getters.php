<?php declare(strict_types = 1);

namespace RenderScript\Extension\ComponentData\Php\_Class;

use RenderScript\Lib\Component\Data as ComponentData;

class Getters extends ComponentData
{
    private GettersDefault $default;

    private array $getters = [];

    public function __construct(string $componentTitle = null)
    {
        $this->default = new GettersDefault();

        parent::__construct($componentTitle);
    }

    public function setDefault(GettersDefault $Default): self
    {
        $this->default = $Default;

        return $this;
    }

    public function getDefault(): GettersDefault
    {
        return $this->default;
    }

    public function Getter()
    {
        return new Getter();
    }

    public function addGetter(Getter $Getter = null): self
    {
        $Getter = ($Getter === null) ? new Getter() : $Getter;

        $this->getters[] = $Getter;
        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'default' => $this->default,
            'getters' => $this->getters,
        ];
    }

}