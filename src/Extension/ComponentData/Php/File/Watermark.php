<?php declare(strict_types=1);

namespace RenderScript\Extension\ComponentData\Php\File;

use RenderScript\Lib\Component\Data as ComponentData;

class Watermark extends ComponentData
{
    public function watermark(string $value): self
    {
        $this->set('watermark', $value);

        return $this;
    }
}