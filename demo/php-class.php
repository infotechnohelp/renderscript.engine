<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use RenderScript\Extension\ComponentData\Php\_Class\Base as PhpClassBase;
use RenderScript\Extension\ComponentData\Php\_Class\Getters;
use RenderScript\Extension\ComponentData\Php\_Class\Properties;
use RenderScript\Extension\ComponentData\Php\_Class\Setters;
use RenderScript\Extension\ComponentData\Php\File\_Namespace as PhpFileNamespace;
use RenderScript\Extension\ComponentData\Php\File\_Use as PhpFileUse;
use RenderScript\Extension\ComponentData\Php\File\Base as PhpFileBase;
use RenderScript\Extension\ComponentData\Php\File\Watermark as PhpFileWatermark;
use RenderScript\Extension\Recipe\Php\_Class as PhpClassRecipe;
use RenderScript\Lib\Recipe\Data as RecipeData;
use RenderScript\Lib\TwigRenderer\RenderingSettings;


$Properties = new Properties();
$Getters = new Getters();
$Setters = new Setters();


$isTerminal = isset($argv);

$settings = ($isTerminal) ? null: (new RenderingSettings())->escapePhpTag();

$contents = (new PhpClassRecipe())->render(
    (new RecipeData())
        ->addComponentData(
            (new PhpFileBase())->declareStrict()
        )
        ->addComponentData(
            (new PhpFileNamespace())->namespace("Custom\\Namespace\\Here")
        )
        ->addComponentData(
            (new PhpFileWatermark())->watermark('Custom watermark')
        )
        ->addComponentData(
            (new PhpFileUse())->addUse("Any\\Required\\Library")
        )
        ->addComponentData(
            (new PhpClassBase())
                ->title('MyLibrary')
                ->extends('Library')
                ->implements('AnyInterface')
                ->abstract()
        )
        ->addComponentData(
            $Properties
                ->setDefault($Properties->getDefault()->visibility('public'))
                ->addProperty('id')
                ->addProperty('counter',
                    $Properties->Property()
                        ->visibility('private')
                        ->static()
                        ->value(0)
                )
                ->addProperty('title',
                    $Properties->Property()
                        ->value("'Product'")
                )
        )
        ->addComponentData(
            $Getters
                ->addGetter($Getters->Getter()
                    ->property('title')
                )
        )
        ->addComponentData(
            $Setters
                ->addSetter(
                    $Setters->Setter()
                        ->property('title')
                        ->returnSelf()
                        ->inputType('string')
                        ->nullableInput()
                )
        ),
    $settings
);

if($isTerminal){
    echo $contents;
    exit;
}

echo "<pre>$contents</pre>";